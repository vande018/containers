# Instruction to build and push the container on WUR Gitlab

```shell
sudo docker login docker-registry.wur.nl
sudo docker build -t docker-registry.wur.nl/vande018/containers:ubuntu22_04_intel .
sudo docker push docker-registry.wur.nl/vande018/containers:ubuntu22_04_intel
```
