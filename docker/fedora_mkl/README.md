#Docker def file for Intel OneAPI compilers and MKL, using Fedora 35

This container provides:
 * Intel OneAPI HPC toolkit (i.e. Intel compilers, MKL, OpenMP, ...)
 * Fortran prepocessor `fypp`
 * Git
