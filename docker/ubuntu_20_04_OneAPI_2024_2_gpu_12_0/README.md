# Instruction to build and push the container on WUR Gitlab

```shell
TOKEN=<generated_token_in_gitlad_with_read_registry_and_write_registry>
echo "$TOKEN" | sudo docker login docker-registry.wur.nl -u jeremie.vandenplas@wur.nl --password-stdin
sudo docker build -t docker-registry.wur.nl/vande018/containers:ubuntu_20_04_OneAPI_2024_2_gpu_12_0 .
sudo docker push docker-registry.wur.nl/vande018/containers:ubuntu_20_04_OneAPI_2024_2_gpu_12_0
```
