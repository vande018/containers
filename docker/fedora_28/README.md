#Docker def file for Intel OneAPI compilers and MKL, using Fedora 28

This container provides:
 * Intel OneAPI HPC toolkit (i.e. Intel compilers, MKL, OpenMP, ...)
 * Fortran prepocessor `fypp`
 * Git

# Instruction to build and push the container on WUR Gitlab

```shell
sudo docker login docker-registry.wur.nl
sudo docker build -t docker-registry.wur.nl/vande018/containers:fedora28 .
sudo docker push docker-registry.wur.nl/vande018/containers:fedora28
```
