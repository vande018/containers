# Singularity def file for Intel OneAPI HPCKIT, using Ubuntu 18.04

This container provides:
 * Intel OneAPI HPC toolkit (i.e. Intel compilers, MKL, OpenMP, ...)
 * Fortran prepocessor `fypp`
 * Git

## Building the container
```shell
module load singularity/3.8.3
singularity build --remote --sandbox hpckit hpckit.simg
```

## For compiling with the container
```shell
singularity shell hpckit
```

When inside the container shell, first run the following command to setup the
Intel environment:

```shell
source /opt/intel/oneapi/setvars.sh
```

If done properly, the container environment is then ready for compilation using
the Intel HPC toolkit.
